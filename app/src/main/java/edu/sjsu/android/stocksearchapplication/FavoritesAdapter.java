package edu.sjsu.android.stocksearchapplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.net.ssl.HttpsURLConnection;

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.ViewHolder>
{
    private List<String> ticker;
    private List<String> name;
    private List<String> price;
    private List<String> percent;
    private List<String> marketCap;
    String finaljson;
    String myticker;
    Context c;


    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mticker;
        public TextView mname;
        public TextView mPrice;
        public TextView mPercent;
        public TextView mCap;
        public View layout;

        public ViewHolder(View v)
        {
            super(v);
            layout = v;
            mticker = (TextView) v.findViewById(R.id.ticker);
            mname = (TextView) v.findViewById(R.id.name);
            mPrice = (TextView)v.findViewById(R.id.price);
            mPercent = (TextView)v.findViewById(R.id.percent);
            mCap = (TextView)v.findViewById(R.id.marketcap);
        }
    }

    public FavoritesAdapter(List<String> tickers, List<String> names, List<String> prices, List<String> percents, List<String> marketCaps)
    {
        this.ticker = tickers;
        this.name = names;
        this.price = prices;
        this.percent = percents;
        this.marketCap = marketCaps;

    }
    public FavoritesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout_favorites, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        holder.mticker.setText(ticker.get(position));
        holder.mname.setText(name.get(position));
        holder.mPrice.setText("$"+price.get(position));
        holder.mPercent.setText(percent.get(position)+"%");
        holder.mCap.setText("Volume: "+marketCap.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                myticker = ticker.get(position);
                c = view.getContext();
                fetchJSON fd = new fetchJSON(ticker.get(position));
                fd.execute();
            }
        });
    }


    @Override
    public int getItemCount() {
        return ticker.size();
    }
    public class fetchJSON extends AsyncTask<String, Void, Void> {

        private String enter;
        String json;
        public fetchJSON(String entered)
        {
            enter = entered;
        }

        protected void onPreExecute()
        {

        }
        @Override
        protected Void doInBackground(String... strings) {
            String url = "https://api.tiingo.com/iex/?tickers=" + enter + "&token=de3fd47f4639992af9e601769314ad60adc0a5d8";
            HttpsURLConnection conn = null;
            URL myurl = null;
            try {
                myurl = new URL(url);
                conn = (HttpsURLConnection) myurl.openConnection();
                conn.setRequestMethod("GET");
                conn.connect();
                BufferedReader br = new BufferedReader(new InputStreamReader(myurl.openStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                json = sb.toString();
                finaljson = json;
            } catch (MalformedURLException | ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param)
        {
            Intent intent = new Intent(c, StockDetailsActivity.class);
            intent.putExtra("jsonString", finaljson);
            intent.putExtra("entered", myticker);
            c.startActivity(intent);

        }
    }
}
