package edu.sjsu.android.stocksearchapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Switch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    AutoCompleteTextView txt;
    CheckBox check;
    Switch switcher;
    Button clear;
    Button quote;
    String enteredVal;

    List<String> tickers;
    List<String> names;
    ImageView refresh;

    SharedPreferences myPref;
    SharedPreferences.Editor edit;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    List<String> favTickers;
    List<String> favNames;
    List<String> favPrices;
    List<String> favPercents;
    List<String> favMarketCaps;
    boolean switchState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        switcher = (Switch) findViewById(R.id.switch1);
        switchState = false;
        refresh = (ImageView) findViewById(R.id.imageView);
        refresh.setImageResource(R.drawable.refresh);

        recyclerView = (RecyclerView) findViewById(R.id.favorites);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        myPref = getPreferences(Context.MODE_PRIVATE);
        edit = myPref.edit();
        //edit.clear();
        //edit.apply();

        Map<String, ?> mkeys = myPref.getAll();
        tickers = new ArrayList<>();
        names = new ArrayList<>();
        check = (CheckBox) findViewById(R.id.checkBox2);
        txt = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        txt.setThreshold(3);
        favTickers = new ArrayList<String>();
        favNames = new ArrayList<String>();
        favPrices = new ArrayList<String>();
        favPercents = new ArrayList<String>();
        favMarketCaps = new ArrayList<String>();

        for (Map.Entry<String, ?> tickAndNameentry : mkeys.entrySet()) {
            favTickers.add(tickAndNameentry.getKey());
            favNames.add(tickAndNameentry.getValue().toString());
        }
        favoritesTask ft = new favoritesTask();
        ft.execute();


        txt = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        txt.setThreshold(3);
        txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String s = txt.getText().toString();
                if (txt.getText().toString().length() >= 3 && !(s.substring(s.length() - 1, s.length())).equals("")) {
                    if (!txt.getText().toString().equals("")) {
                        fetchAutocomplete fa = new fetchAutocomplete(txt.getText().toString());
                        txt.requestFocus();
                        fa.execute();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favoritesTask ft = new favoritesTask();
                ft.execute();
            }
        });

        switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                autoRefresh ar = new autoRefresh();
                if (b == true) {
                    ar.execute();
                }
            }
        });

        clear = (Button) findViewById(R.id.button);
        clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                txt.setText("");
            }
        });
        quote = (Button) findViewById(R.id.button2);
        quote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = txt.getText().toString();
                if (txt.getText().toString().length() == 0) {
                    Context c = view.getContext();
                    AlertDialog.Builder builder = new AlertDialog.Builder(c);
                    builder.setTitle("INVALID");
                    builder.setMessage("Please enter a stock symbol or name.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    builder.create();
                    builder.show();
                } else if (s.charAt(0) != '{' && s.charAt(s.length() - 1) != '}') {
                   s = s.toUpperCase();
                    if (!tickers.contains(s) && !names.contains(s)) {
                        Context c = view.getContext();
                        AlertDialog.Builder builder = new AlertDialog.Builder(c);
                        builder.setTitle("INVALID SYMBOL OR NAME");
                        builder.setMessage("Please enter a valid stock symbol or name.")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.create();
                        builder.show();
                    } else {
                        enteredVal = s;
                        int index = tickers.indexOf(enteredVal);
                        if (check.isChecked()) {
                            edit.putString(enteredVal, names.get(index));
                            edit.apply();
                            favTickers.add(enteredVal);
                            favNames.add(names.get(index));
                            favoritesTask2 f2 = new favoritesTask2();
                            f2.execute();

                        }
                        fetchJSON fd = new fetchJSON(s);
                        fd.execute();
                    }
                } else if (s.charAt(0) == '{' && s.charAt(s.length() - 1) == '}') {
                    int equalSignIndex = s.indexOf("=");
                    int commaIndex = s.indexOf(",");
                    String entered = s.substring(equalSignIndex + 1, commaIndex);
                    String name = s.substring(commaIndex + 7, s.indexOf("}"));
                    enteredVal = entered;
                    if (check.isChecked()) {
                        edit.putString(enteredVal, name);
                        edit.apply();
                        favTickers.add(enteredVal);
                        favNames.add(name);
                        favoritesTask2 ft2 = new favoritesTask2();
                        ft2.execute();
                    }
                    fetchJSON fd = new fetchJSON(entered);
                    fd.execute();
                }

            }
        });

    }


    public class fetchJSON extends AsyncTask<String, Void, Void> {
        private final ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        String waitMsg = "Fetching Data...";
        private String enter;
        String json;

        public fetchJSON(String entered) {
            enter = entered;
        }

        protected void onPreExecute() {
            this.dialog.setMessage(waitMsg);
            this.dialog.setCancelable(false);
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            String url = "https://api.tiingo.com/iex/?tickers=" + enter + "&token=de3fd47f4639992af9e601769314ad60adc0a5d8";
            HttpsURLConnection jconn = null;
            URL myurl = null;
            try {
                myurl = new URL(url);
                jconn = (HttpsURLConnection) myurl.openConnection();
                jconn.setRequestMethod("GET");
                jconn.connect();
                BufferedReader buff = new BufferedReader(new InputStreamReader(myurl.openStream()));
                StringBuilder myBuild = new StringBuilder();
                String line;
                while ((line = buff.readLine()) != null)
                {
                    myBuild.append(line + "\n");
                }
                buff.close();
                json = myBuild.toString();
            } catch (MalformedURLException | ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            Intent intent = new Intent(getApplicationContext(), StockDetailsActivity.class);
            intent.putExtra("jsonString", json);
            intent.putExtra("entered", enteredVal);
            startActivity(intent);

        }
    }

    public class fetchAutocomplete extends AsyncTask<String, Void, Void> {
        private String enter;
        String json;

        public fetchAutocomplete(String entered) {
            enter = entered;
        }

        protected void onPreExecute() {
            tickers.clear();
            names.clear();
        }

        @Override
        protected Void doInBackground(String... strings) {
            String url = "https://api.tiingo.com/tiingo/utilities/search?query=" + enter + "&token=de3fd47f4639992af9e601769314ad60adc0a5d8";
            HttpsURLConnection jconn = null;
            URL myurl = null;
            try {
                myurl = new URL(url);
                jconn = (HttpsURLConnection) myurl.openConnection();
                jconn.setRequestMethod("GET");
                jconn.connect();
                BufferedReader buff = new BufferedReader(new InputStreamReader(myurl.openStream()));
                StringBuilder myBuilder = new StringBuilder();
                String line;
                while ((line = buff.readLine()) != null) {
                    myBuilder.append(line + "\n");
                }
                buff.close();
                json = myBuilder.toString();
                JSONArray j = new JSONArray(json);
                for (int i = 0; i < j.length(); i++) {
                    JSONObject k = j.getJSONObject(i);
                    tickers.add(k.get("ticker").toString());
                    names.add(k.get("name").toString());

                }
            } catch (MalformedURLException | ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param) {
            List<Map<String, String>> tickerandnamelist = new ArrayList<Map<String, String>>();
            Map<String, String> tickandnamemap;

            for (int i = 0; i < names.size() && i < tickers.size(); i++) {
                tickandnamemap = new HashMap<String, String>();
                tickandnamemap.put("ticker", tickers.get(i));
                tickandnamemap.put("name", names.get(i));
                tickerandnamelist.add(tickandnamemap);
            }
            SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), tickerandnamelist, R.layout.row_layout, new String[]{"ticker", "name"}, new int[]{R.id.icon, R.id.text});
            txt.setAdapter(adapter);
            if (txt.getText().length() >= 3) {
                txt.showDropDown();
            }
        }
    }


    public class favoritesTask extends AsyncTask<Void, Void, Void> {
        private String enter;
        String json;
        String s;

        protected void onPreExecute() {
            favPrices.clear();
            favPercents.clear();
            favMarketCaps.clear();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (int i = 0; i < favTickers.size(); i++) {
                s = favTickers.get(i);
                String url = "https://api.tiingo.com/iex/?tickers=" + s + "&token=de3fd47f4639992af9e601769314ad60adc0a5d8";
                HttpsURLConnection jconn = null;
                URL myurl = null;
                try {
                    myurl = new URL(url);
                    jconn = (HttpsURLConnection) myurl.openConnection();
                    jconn.setRequestMethod("GET");
                    jconn.connect();
                    BufferedReader buff = new BufferedReader(new InputStreamReader(myurl.openStream()));
                    StringBuilder myBuilder = new StringBuilder();
                    String line;
                    while ((line = buff.readLine()) != null) {
                        myBuilder.append(line + "\n");
                    }
                    buff.close();
                    json = myBuilder.toString();
                    JSONArray j = new JSONArray(json);
                    JSONObject k = j.getJSONObject(0);
                    double percent = ((k.getDouble("last") - k.getDouble("prevClose")) / k.getDouble("prevClose")) * 100;
                    if (percent < 0) {
                        favPercents.add(i, new String("-" + percent));
                    } else {
                        String s = "+" + percent;
                        favPercents.add(i, s);
                    }

                    favMarketCaps.add(i, k.get("volume").toString());
                    favPrices.add(i, k.get("last").toString());

                } catch (MalformedURLException | ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void param) {
            mAdapter = new FavoritesAdapter(favTickers, favNames, favPrices, favPercents, favMarketCaps);
            recyclerView.setAdapter(mAdapter);

        }
    }

    public class favoritesTask2 extends AsyncTask<Void, Void, Void> {
        private String enter;
        String json;
        String s;

        protected void onPreExecute() {

            s = favTickers.get(favTickers.size() - 1);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String url = "https://api.tiingo.com/iex/?tickers=" + s + "&token=de3fd47f4639992af9e601769314ad60adc0a5d8";
            HttpsURLConnection jconn = null;
            URL myurl = null;
            try {
                myurl = new URL(url);
                jconn = (HttpsURLConnection) myurl.openConnection();
                jconn.setRequestMethod("GET");
                jconn.connect();
                BufferedReader buff = new BufferedReader(new InputStreamReader(myurl.openStream()));
                StringBuilder myBuilder = new StringBuilder();
                String line;
                while ((line = buff.readLine()) != null) {
                    myBuilder.append(line + "\n");
                }
                buff.close();
                json = myBuilder.toString();
                JSONArray j = new JSONArray(json);
                JSONObject k = j.getJSONObject(0);
                double percent = (((k.getDouble("last") - k.getDouble("prevClose")) / k.getDouble("prevClose"))) * 100;
                if (percent < 0) {
                    favPercents.add(new String("-" + percent));
                } else {
                    String s = "+" + percent;
                    favPercents.add(s);
                }

                favMarketCaps.add(k.get("volume").toString());
                favPrices.add(k.get("last").toString());

            } catch (MalformedURLException | ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param) {
            mAdapter = new FavoritesAdapter(favTickers, favNames, favPrices, favPercents, favMarketCaps);
            recyclerView.setAdapter(mAdapter);
        }
    }


    int autocounter = 0;
    public class autoRefresh extends AsyncTask<Void, Void, Void>
    {
        String json;
        String s;
        List<String> percents;
        List<String> prices;
        List<String> mcaps;
        protected void onPreExecute()
        {
            percents = new ArrayList<>();
            prices = new ArrayList<>();
            mcaps = new ArrayList<>();

        }

        @Override
        protected Void doInBackground(Void... voids) {
            autocounter++;
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < favTickers.size(); i++)
            {
                s = favTickers.get(i);
                String url = "https://api.tiingo.com/iex/?tickers=" + s + "&token=de3fd47f4639992af9e601769314ad60adc0a5d8";
                HttpsURLConnection jconn = null;
                URL myurl = null;
                try {
                    myurl = new URL(url);
                    jconn = (HttpsURLConnection) myurl.openConnection();
                    jconn.setRequestMethod("GET");
                    jconn.connect();
                    BufferedReader buff = new BufferedReader(new InputStreamReader(myurl.openStream()));
                    StringBuilder myBuilder = new StringBuilder();
                    String line;
                    while ((line = buff.readLine()) != null) {
                        myBuilder.append(line + "\n");
                    }
                    buff.close();
                    json = myBuilder.toString();
                    JSONArray j = new JSONArray(json);
                    JSONObject k = j.getJSONObject(0);
                    double percent = ((k.getDouble("last") - k.getDouble("prevClose")) / k.getDouble("prevClose")) * 100;
                    if (percent < 0)
                    {
                        percents.add(i, new String("-" + percent));
                    }
                    else {
                        String s = "+" + percent;
                        percents.add(i, s);
                    }

                    mcaps.add(i, k.get("volume").toString());
                    prices.add(i, k.get("last").toString());

                } catch (MalformedURLException | ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void param)
        {
            mAdapter = new FavoritesAdapter(favTickers, favNames, prices, percents, mcaps);
            recyclerView.setAdapter(mAdapter);

            if (switcher.isChecked()==true)
            {
                autoRefresh ar = new autoRefresh();
                ar.execute();
            }

        }
    }


}


