package edu.sjsu.android.stocksearchapplication;

import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class CurrentAdapter extends RecyclerView.Adapter<CurrentAdapter.ViewHolder>
{
    private List<String> keys;
    private List<String> values;


    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView textKey;
        public TextView textValue;
        public View layout;

        public ViewHolder(View v)
        {
            super(v);
            layout = v;
            textKey = (TextView) v.findViewById(R.id.icon);
            textValue = (TextView) v.findViewById(R.id.text);
        }
    }

    public CurrentAdapter(List<String> keyList, List<String> valList)
    {
        this.keys = keyList;
        this.values = valList;
    }
    public CurrentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        String myKey = keys.get(position);
        holder.textKey.setText(myKey);
        String myVal = values.get(position);
        holder.textValue.setText(myVal);

    }


    @Override
    public int getItemCount() {
        return keys.size();
    }
}
