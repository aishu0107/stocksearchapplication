package edu.sjsu.android.stocksearchapplication;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;

public class StockDetailsActivity extends AppCompatActivity
{
    List<String> currentStockkey = new ArrayList<String>();
    List<String> currentStockval = new ArrayList<String>();

    List<String> historicStockkey = new ArrayList<String>();
    List<String> historicStockval = new ArrayList<String>();

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;

    private RecyclerView recyclerView2;
    private RecyclerView.Adapter mAdapter2;

    private RecyclerView.LayoutManager layoutManager2;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_details);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView2 = (RecyclerView)findViewById(R.id.my_recycler_view2);
        recyclerView2.setHasFixedSize(true);
        layoutManager2 = new LinearLayoutManager(this);
        recyclerView2.setLayoutManager(layoutManager2);

        Bundle myInput = this.getIntent().getExtras();
        String val = myInput.getString("jsonString");
        String tick = myInput.getString("entered");
        DisplayCurrent dc = new DisplayCurrent();
        dc.execute(val);
        DisplayHistoric dh = new DisplayHistoric();
        dh.execute(tick);
    }
    public class DisplayCurrent extends AsyncTask<String, Void, Void> {
        private final ProgressDialog dialog = new ProgressDialog(StockDetailsActivity.this);
        String waitMsg = "Fetching Data...";
        protected void onPreExecute()
        {
            this.dialog.setMessage(waitMsg);
            this.dialog.setCancelable(false);
            this.dialog.show();
        }
        @Override
        protected Void doInBackground(String... strings)
        {
            String json = strings[0];
            try
            {
                JSONArray j = new JSONArray(json);
                JSONObject obj = j.getJSONObject(0);
                Iterator<String> objIter = obj.keys();
                while (objIter.hasNext())
                {
                    String key = objIter.next();
                    currentStockkey.add(key);
                    if (obj.get(key).equals(null))
                    {
                        currentStockval.add("not available");
                    }
                    else
                    {
                        currentStockval.add(obj.get(key).toString());
                    }

                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Void param)
        {
            mAdapter = new CurrentAdapter(currentStockkey,currentStockval);
            recyclerView.setAdapter(mAdapter);
            if (this.dialog.isShowing())
            {
                this.dialog.dismiss();
            }
        }

    }


    public class DisplayHistoric extends AsyncTask<String, Void, Void> {
        private final ProgressDialog dialog = new ProgressDialog(StockDetailsActivity.this);
        String waitMsg = "Fetching Data...";
        protected void onPreExecute()
        {
            this.dialog.setMessage(waitMsg);
            this.dialog.setCancelable(false);
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings)
        {
           String s = strings[0];
           String url = "https://api.tiingo.com/tiingo/daily/"+s+"/prices?startDate=01/01/2020&resampleFreq=monthly&token=de3fd47f4639992af9e601769314ad60adc0a5d8";
            HttpsURLConnection jconn = null;
            URL myurl = null;
            try {
                myurl = new URL(url);
                jconn = (HttpsURLConnection) myurl.openConnection();
                jconn.setRequestMethod("GET");
                jconn.connect();
                BufferedReader buff = new BufferedReader(new InputStreamReader(myurl.openStream()));
                StringBuilder myBuilder = new StringBuilder();
                String line;
                while ((line = buff.readLine()) != null) {
                    myBuilder.append(line + "\n");
                }
                buff.close();
                String json = myBuilder.toString();

                JSONArray arr = new JSONArray(json);
                for (int i = 0;i<arr.length();i++)
                {
                    JSONObject obj = arr.getJSONObject(i);
                    Iterator<String> objIter = obj.keys();
                    while (objIter.hasNext())
                    {
                        String key = objIter.next();
                        historicStockkey.add(key);
                        if (obj.get(key).equals(null))
                        {
                            historicStockval.add("not available");
                        }
                        else
                        {
                            historicStockval.add(obj.get(key).toString());
                        }

                    }
                }
            } catch (MalformedURLException | ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(Void param)
        {
            mAdapter2 = new CurrentAdapter(historicStockkey,historicStockval);
            recyclerView2.setAdapter(mAdapter2);
            if (this.dialog.isShowing())
            {
                this.dialog.dismiss();
            }
        }

    }

}